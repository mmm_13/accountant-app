import copy

from flask import Flask, render_template, request
from accountant import mg, SavePrzeglad

app = Flask(__name__)


def update_main_page():
    balance = mg.balance
    products = mg.products
    products_list = []
    for product in products:
        element = products[product]
        products_list.append((element.name, element.amount))
    return balance, products_list


def modify_results(results_list):
    new_list = []
    while results_list:
        temp_list = []
        for i in range(2):
            temp_list.append((results_list.pop(0)))
        new_list.append(temp_list)
    return new_list


templates_dict = {'saldo': 'saldo.html', 'zakup': 'zakup.html', 'sprzedaz': 'sprzedaz.html'}


@app.route('/')
def start_app():
    balance, products_list = update_main_page()
    return render_template('main_page.html', balance=balance, products=products_list)


@app.route('/main/')
def main_page():
    balance, products_list = update_main_page()
    return render_template('main_page.html', balance=balance, products=products_list)


@app.route('/saldo/')
def saldo_page():
    return render_template('saldo.html')


@app.route('/sprzedaz/')
def sprzedaz_page():
    return render_template('sprzedaz.html')


@app.route('/zakup/')
def zakup_page():
    return render_template('zakup.html')


@app.route('/historia/')
def historia_page():
    saver = SavePrzeglad()
    results = saver.make_results(1, len(mg.actions))
    results = modify_results(results)
    return render_template('historia.html', results=results)


@app.route('/historia/resp', methods=['post'])
def historia_resp_page():
    resp_dict = dict(request.form)
    first_idx = resp_dict["first_idx"]
    second_idx = resp_dict["second_idx"]
    saver = SavePrzeglad()
    results = saver.make_results(first_idx, second_idx)
    results = modify_results(results)
    return render_template('historia.html', results=results)


@app.route('/historia/<first_idx>/<second_idx>/')
def historia_page_idx(first_idx, second_idx):
    saver = SavePrzeglad()
    results = saver.make_results(first_idx, second_idx)
    results = modify_results(results)
    return render_template('historia.html', results=results)


@app.route('/resp/', methods=['post'])
def resp_page():
    resp_dict = dict(request.form)
    params = []
    for i in resp_dict.values():
        params.append(i)
    action = params[0]
    action_results = mg.callbacks[action](params[1:])
    if action_results == 'OK':
        mg.temporary_actions = copy.copy(params)
        mg.update_store()
        balance, products_list = update_main_page()
        return render_template('main_page.html', balance=balance, products=products_list)
    else:
        template = templates_dict[action]
        return render_template(template, error=action_results)


if __name__ == '__main__':
    app.run(debug=True)
