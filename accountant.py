"""
Nowy ulepszony pakiet funkcji i klas do obsługi systemu ksiegowego.
Zastosowano reguły SOLID.
"""
import sys
import os
from pathlib import Path
import copy


class Product:

    def __init__(self, name):
        self.name = name
        self.amount = 0


class Manager:

    def __init__(self):
        self.callbacks = {}
        self.action_args = {}
        self.balance = 0
        self.temporary_actions = []
        self.actions = []
        self.products = {}

    def assign(self, action, count):
        def decorate(callback):
            self.callbacks[action] = callback
            self.action_args[action] = count

        return decorate

    def update_store(self):
        while self.temporary_actions:
            action = self.temporary_actions.pop(0)
            params = []
            if action in self.action_args:
                for i in range(self.action_args[action]):
                    params.append(self.temporary_actions.pop(0))
                self.actions.append((action, params))

    def execute(self):
        for action, params in self.actions:
            if action in self.callbacks:
                self.callbacks[action](params)


class ReadActions:

    def __init__(self, import_file):
        self.import_file = import_file
        self.actions_list = []

    def import_data_to_list(self):
        if os.path.isfile(self.import_file) and Path(self.import_file).suffix == '.txt':
            with open(self.import_file, 'r', encoding='utf8', newline='') as file:
                for line in file:
                    self.actions_list.append(line.strip())
        else:
            print('Wskazana przez użytkownika ścieżka {} nie jest plikiem'
                  ' txt lub nie istnieje.'.format(self.import_file))
            sys.exit('Import danych niemożliwy.')


import_file = "sprzedaz_in.txt"
mg = Manager()
reader = ReadActions(import_file)
reader.import_data_to_list()


def zero_price_amount_test(price, amount):
    if price <= 0:
        info = 'Błąd - cena produktu musi być większa niż 0 gr.'
        return info
    elif amount <= 0:
        info = 'Błąd - liczba sztuk produktu musi być większa od 0.'
        return info
    return 'OK'


def purchase_test(price, amount):
    if price * amount > mg.balance:
        info = 'Błąd - całkowita kwota zakupu przekracza wartość salda.'
        return info
    return 'OK'


def sell_test(name, amount):
    if name not in mg.products:
        info = "Błąd - wybrany produkt nie jest dostępny na magazynie."
        return info
    elif amount > mg.products[name].amount:
        info = "Błąd - produkt w wybranej ilości sztuk nie jest dostępny na magazynie."
        return info
    return 'OK'


@mg.assign('saldo', 2)
def saldo(params):
    quota = int(params[0])
    mg.balance += quota
    return 'OK'


@mg.assign('zakup', 3)
def buy_product(params):
    name = params[0]
    price = int(params[1])
    amount = int(params[2])
    test = zero_price_amount_test(price, amount)
    if test == 'OK':
        test = purchase_test(price, amount)
        if test == 'OK':
            if name not in mg.products:
                product = Product(name)
                mg.products[name] = product
                mg.products[name].amount = amount
            else:
                mg.products[name].amount += amount
            mg.balance -= price * amount
    return test


@mg.assign('sprzedaz', 3)
def buy_product(params):
    name = params[0]
    price = int(params[1])
    amount = int(params[2])
    test = zero_price_amount_test(price, amount)
    if test == 'OK':
        test = sell_test(name, amount)
        if test == 'OK':
            mg.products[name].amount -= amount
            mg.balance += price * amount
    return test


mg.temporary_actions = copy.copy(reader.actions_list)
mg.update_store()
mg.execute()


class SaveResults:

    def __init__(self):
        self.results_list = []


class SaveActions(SaveResults):

    def make_results(self):
        self.results_list = copy.copy(reader.actions_list)
        return self.results_list


class SaveKonto(SaveResults):

    def make_results(self):
        self.results_list = [mg.balance]
        return self.results_list


class SaveMagazyn(SaveResults):

    def make_results(self):
        results = []
        for element in reader.in_args:
            if element in mg.products:
                info = '{} : {}'.format(element, mg.products[element].amount)
            else:
                info = '{} : {}'.format(element, 0)
            results.append(info)
        self.results_list = results
        return self.results_list


class SavePrzeglad(SaveResults):

    def make_results(self, first_idx, second_idx):
        results = []
        first_idx = int(first_idx)
        last_idx = int(second_idx)
        if first_idx > len(mg.actions):
            first_idx = len(mg.actions)
            last_idx = len(mg.actions)
        elif last_idx >= len(mg.actions):
            last_idx = len(mg.actions)
        for i in range(first_idx-1, last_idx):
            for x in mg.actions[i]:
                results.append(x)
        self.results_list = results
        return self.results_list

